import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatExpansionModule} from '@angular/material/expansion';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { RouterModule, Routes } from '@angular/router';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatCardModule} from '@angular/material/card';
import {FormsModule }   from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AngularFireAuth } from '@angular/fire/auth';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ClassifiedComponent } from './classified/classified.component';
import { DocFormComponent } from './doc-form/doc-form.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { PostsComponent } from './posts/posts.component';
import { SavedPostsComponent } from './saved-posts/saved-posts.component';

 
const appRoutes: Routes = [
 // { path: 'books', component: BooksComponent },
 { path: 'signup', component: SignUpComponent },
  { path: 'login', component: LoginComponent },
  { path: 'docform', component: DocFormComponent },
  { path: 'classified', component: ClassifiedComponent },
  { path: 'welcome', component:WelcomeComponent },
  { path: 'posts', component:PostsComponent },
  { path: 'saved', component:SavedPostsComponent },
  { path: "",
  redirectTo: '/welcome',
  pathMatch: 'full'
},
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    SignUpComponent,
    ClassifiedComponent,
    DocFormComponent,
    WelcomeComponent,
    PostsComponent,
    SavedPostsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    FormsModule,
    HttpClientModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig,'angularExam20'),
    AngularFireModule,
    AngularFirestoreModule,
    RouterModule.forRoot(
      appRoutes,
     { enableTracing: true } // <-- debugging purposes only
    )    


  ],
  providers: [AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email:string;
  password:string;

  constructor(public authservice:AuthService,private route:ActivatedRoute, public router:Router) { }

  ngOnInit() {
  }
  
  onSubmit(){
  
    this.authservice.login(this.email,this.password)
    
  }

}

import { Post } from './interfaces/post';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  postCollection:AngularFirestoreCollection=this.db.collection('posts');
  userCollection:AngularFirestoreCollection=this.db.collection('users');
  apiurl="https://jsonplaceholder.typicode.com/posts";
  constructor(private http:HttpClient,private db:AngularFirestore) { }

  getPost(){ 
       return this.http.get<Post[]>(this.apiurl);
      }

     savePost(userId:string,title_input:string, body_input:string)
  {
    const post={title:title_input,body:body_input};
    this.userCollection.doc(userId).collection('posts').add(post);
  }
  
  getPosts(userId:string):Observable<any[]>{
    // return this.postCollection.valueChanges({idField:'id'}); 
      this.postCollection= this.db.collection(`users/${userId}/posts`);
      return this.postCollection.snapshotChanges().pipe(
        //pipe מאפשר לשרשר פונקציות
        map(
          //map גם מאפשר לשרשר פונקציות
          collection => collection.map(
            document => {
              const data = document.payload.doc.data();
              data.id= document.payload.doc.id;
              console.log(data);
              return data;
            }
          )
        )
      )
           }

           deletePost(id:string,userId:string){
            this.db.doc(`users/${userId}/posts/${id}`).delete();
      }    

          

}
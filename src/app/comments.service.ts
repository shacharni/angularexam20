import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  
  apiurl="https://jsonplaceholder.typicode.com/comments";

  constructor(private http:HttpClient) { }


  getComment(){ 
    return this.http.get<Comment[]>(this.apiurl);
   }
}

  
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { PostsService } from '../posts.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-saved-posts',
  templateUrl: './saved-posts.component.html',
  styleUrls: ['./saved-posts.component.css']
})
export class SavedPostsComponent implements OnInit {

  postsD:Observable<any>;
userId:string;

  constructor(public authService:AuthService,public postservice:PostsService) { }

  ngOnInit(){
    
      this.authService.user.subscribe(
              user => {
                this.userId = user.uid;
               this.postsD=this.postservice.getPosts(this.userId);      }
           )
  }
  deleteBook(id:string){
    this.postservice.deletePost(id,this.userId); 

  }


}

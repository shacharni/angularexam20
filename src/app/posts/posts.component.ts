import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Post } from '../interfaces/post';
import { post } from 'selenium-webdriver/http';
import { User } from '../interfaces/user';
import { AuthService } from '../auth.service';
import { CommentsService } from '../comments.service';
//import { UsersService } from '../users.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  userId:string;
  Post$:Post[];
 comment$:Comment[];
  saveText:string="save to data base";
 
 // User$:Users[];
  constructor(private postsservis:PostsService,public authService:AuthService,public commentService:CommentsService) { }

  ngOnInit() {
   
    this.postsservis.getPost().subscribe(data=>this.Post$=data);
    this.commentService.getComment().subscribe(data=>this.comment$=data);
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        }
    )
  
  }
 
 onSubmit(id:number){
  console.log(this.userId)
  this.saveText="saved for later"
    this.postsservis.savePost(this.userId,this.Post$[id].title,this.Post$[id].body);
  }

}
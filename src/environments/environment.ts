// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebaseConfig :{
    apiKey: "AIzaSyBFroSxtd0akViN_hF02QCbRZk6FsTalZ8",
    authDomain: "angularexam20.firebaseapp.com",
    databaseURL: "https://angularexam20.firebaseio.com",
    projectId: "angularexam20",
    storageBucket: "angularexam20.appspot.com",
    messagingSenderId: "462564881428",

  
}
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
